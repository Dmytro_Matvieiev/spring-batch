package lab4.spring.batch.example.service;

import lab4.spring.batch.example.domain.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Service;

@Service
public class UserFilterProcessor implements ItemProcessor<User, User> {

    @Override
    public User process(User user) throws Exception {
        return user.isBadBalance() ? user : null;
    }
}
