package lab4.spring.batch.example.service;

import lab4.spring.batch.example.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserItemWriter implements ItemWriter<User> {

    private JavaMailSender javaMailSender;

    public UserItemWriter(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void write(List<? extends User> list) throws Exception {
        list.forEach(user -> {
            SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
            simpleMailMessage.setTo(user.getEmail());
            simpleMailMessage.setSubject("Low balance");
            simpleMailMessage.setText("Hello! Dear " + user.getFullName() + ", you received this message because your balance is too low.");
            javaMailSender.send(simpleMailMessage);
            log.info("Email successfully sent to: {} with email address: {}", user.getFullName(), user.getEmail());
        });
    }
}
