package lab4.spring.batch.example;

import lab4.spring.batch.example.domain.User;
import lab4.spring.batch.example.service.UserFilterProcessor;
import lab4.spring.batch.example.service.UserItemWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.mail.javamail.JavaMailSender;

import javax.sql.DataSource;
import java.math.BigDecimal;

@Slf4j
@EnableBatchProcessing
@SpringBootApplication
public class SpringBatchApplication {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	@Autowired
	private StepBuilderFactory stepBuilderFactory;
	@Autowired
	private JavaMailSender javaMailSender;
	@Autowired
	private DataSource dataSource;

	private static final String GET_ALL_USERS = "select id, full_name, email, balance from user";

	public static void main(String[] args) {
		SpringApplication.run(SpringBatchApplication.class, args);
	}

	@Bean
	public RowMapper<User> rowMapper() {
		return (resultSet, rowNum) -> {
			Long id = resultSet.getLong("id");
			String fullName = resultSet.getString("full_name");
			String email = resultSet.getString("email");
			BigDecimal balance = resultSet.getBigDecimal("account.balance");
			return new User(id, fullName, email, balance);
		};
	}

	@Bean
	public ItemReader<User> itemReader(DataSource dataSource, RowMapper<User> rowMapper) {
		JdbcCursorItemReader<User> itemReader = new JdbcCursorItemReader<>();
		itemReader.setDataSource(dataSource);
		itemReader.setSql(GET_ALL_USERS);
		itemReader.setRowMapper(rowMapper);
		return itemReader;
	}

	@Bean
	public ItemWriter<User> itemWriter() {
		return new UserItemWriter(javaMailSender);
	}

	@Bean
	public ItemProcessor<User, User> itemProcessor() {
		return new UserFilterProcessor();
	}

	@Bean
	public Job userJob() {
		return jobBuilderFactory.get("userJob")
				.incrementer(new RunIdIncrementer())
				.flow(step())
				.end()
				.build();
	}

	@Bean
	public Step step() {
		return stepBuilderFactory.get("step")
				.<User, User>chunk(10)
				.reader(itemReader(dataSource, rowMapper()))
				.processor(itemProcessor())
				.writer(itemWriter())
				.build();
	}
}
