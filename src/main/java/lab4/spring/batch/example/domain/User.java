package lab4.spring.batch.example.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Long id;
    private String fullName;
    private String email;
    private BigDecimal balance;

    private static final BigDecimal borderBalance = new BigDecimal(10);

    public boolean isBadBalance() {
        return balance.compareTo(borderBalance) < 0;
    }
}
